jQuery(document).ready(init());

function init(){
	console.log("init()");
	let pathSplited = window.location.pathname.split("/");
	if("post" == pathSplited[2]){
		if("index" == pathSplited[3]){
			console.log("on ads index");
      adsIndex();
		}else if(parseInt(Number(pathSplited[3])) == pathSplited[3]){
			console.log("on ads " + pathSplited[3] + " details");
			adsDetails(pathSplited[3]);
		}
	}
}

function adsDetails(adsId){
	console.log("css", browser.extension.getURL("/css/adsDetails.css"));
	jQuery("head").append('<link href="'
		+ browser.extension.getURL("/css/adsDetails.css")
		+ '" rel="stylesheet">');
	if(jQuery("#notes").length < 1){
		jQuery("tbody").append('<tr class="notes" data-key="' + adsId + '"><th>Notes<span><img src="'
			+ browser.extension.getURL("/icons/pencil-plus.png")
			+ '"/></span></th><td id="notes"></td></tr>');
	}
  jQuery("tr.notes>th>span").click(addNoteForm);
	let notes = load(adsId);
	if(false != notes){
		let html = drowNotes(notes);
		console.log(html);
		jQuery("td#notes").append(html);
	}
}

function adsIndex(){
  jQuery("head").append('<link href="'
    + browser.extension.getURL("/css/adsIndex.css")
    + '" rel="stylesheet">');
  jQuery("thead>tr").append('<th>Notes</th>');
  jQuery("tbody>tr").each(function( index, row ) {
		let adsId = jQuery(row).data("key");
    // console.log("adsIndex append to", jQuery(row).data("key"));
		let notes = load(adsId);
		let nbNotes = 0;
		if(false != notes){
			nbNotes =  notes.length;
		}
    jQuery(row).append('<td class="notes">' + nbNotes + '<img src="'
      + browser.extension.getURL("/icons/pencil.png") +'"/></td>');
  });
}

function load(adsId){
	//console.warn("Load from local storage " + adsId);
	let storage = localStorage.getItem(adsId);
	if(storage == null){return false; };
	let json = JSON.parse(storage);
	//console.log(json);
	return json.notes;
}

function save(adsId, note){
	console.warn("Save to local storage " + adsId + " : " + note);
	let storage = localStorage.getItem(adsId);
  console.debug("storage", storage);
  let json;
	if(null == storage){
    // save new
    json = {notes:[{date:Date.now(),text:note},]};
  }else{
    // add to existing
    json = JSON.parse(storage);
		json.notes.push({date:Date.now(),text:note});
  }
  console.log(json);
  localStorage.setItem(adsId,JSON.stringify(json));
  return json;
}

function addNoteForm(){
  let adsId = jQuery("tr.notes").data("key");
  console.log("display form for adding note to " + adsId);
  jQuery("td#notes").append(`<form>
  <label for="noteFormText">add note:</label><br>
  <textarea id="noteFormText" name="noteFormText" rows="4" cols="50"></textarea><br>
  <button type="reset" id="noteFormCancel"><img src="`
			+ browser.extension.getURL("/icons/times-circle.png")
			+ `"/></button>
  <button type="submit" id="noteFormvalid"><img src="`
			+ browser.extension.getURL("/icons/check-circle.png")
			+ `"/></button>
  </form>`);
  jQuery("#noteFormCancel").click(
    function(){
      console.debug("cancel");
      jQuery(this).parent().remove();
    }
  );
  jQuery("#noteFormvalid").click(
    function(){
      console.debug("Valid");
      let note = jQuery("#noteFormText")[0].value;
      save(adsId, note);
    }
  );
}

function drowNotes(notes){
	let html = `<dl>
	`
	notes.forEach(function(note){
		console.log(note, note.date, note.text);
		html += `<dt>` + new Date(note.date).toLocaleDateString() + `</dt>
		<dd>` + note.text + `</dd>
		`;
	});
	html += `</dl>`;
	return html;
}
